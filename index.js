var fs = require("fs");

module.exports = {
	lang : "lang",
	locales_path : __dirname + "/../../locales/",
	url_path : "/",
	locales : ["en"],
	translations : {},
	init : function (conf) {
		if ("locales_path" in conf) {
			this.locales_path = conf.locales_path;
		}
		if ("url_path" in conf) {
			this.url_path = conf.url_path;
		}
		if ("locales" in conf) {
			this.locales = conf.locales;
		}
		this.loadLocales();
		if ("app" in conf) {
			this.bindExpress(conf.app);
		}
	},
	bindExpress : function (app) {
		var to = this;
		app.all(this.url_path, function (req, res, next) {
			if (to.lang in req.query && req.query[to.lang] in to.translations) {
				req.session[to.lang] = req.query[to.lang];
			} else if (!(to.lang in req.session)) {
				req.session[to.lang] = to.locales[0];
			}
			to.buildI18nObject(req);
			next();
		});
	},
	loadLocales : function () {
		var i, locale, json;
		for (i = 0; i < this.locales.length; i++) {
			locale = this.locales[i];
			json = fs.readFileSync(this.locales_path + locale + ".json");
			if (!this.translations[locale]) this.translations[locale] = {};
			Object.assign(this.translations[locale], JSON.parse(json));
		}
	},
	buildI18nObject : function (req) {
		var to = this;
		var i18n = {
			lang : req.session[to.lang],
			t : function (key) {
				return to.t(this.lang, key);
			}
		};
		req.i18n = i18n;
	},
	t : function (lang, key) {
		var source = this.translations[lang];
		if (key in source) {
			return source[key];
		} else if (key in this.translations[this.locales[0]]) {
			return this.translations[this.locales[0]][key];
		} else {
			return key;
		}
	}
};